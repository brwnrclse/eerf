#!/usr/bin/env node
const
  favicons = require(`favicons`),
  fs = require(`fs`),

  faviconSrc = `app/assets/img/icon.png`,
  faviconCfg = {
    appName: `eerf`,
    appDescription: `Writing...unhinged`,
    developerName: `brwnrclse (Barry Harris)`,
    developerURL: `https://brwnrclse.xyz`,
    path: `/`,
    url: `brwnrclse.xyz/eerf`,
    display: `standalone`,
    orientation: `portrait`,
    version: `1.0`,
    logging: false,
    online: false,
    icons: {
      android: true,
      appleIcon: true,
      appleStartup: true,
      coast: true,
      favicons: true,
      firefox: true,
      opengraph: true,
      twitter: true,
      windows: true,
      yandex: true
    }
  },

  faviconCb = (err, res) => {
    if (err) {
      console.log(JSON.stringify(err));
    }
    res.images.map((c) => {
      fs.writeFile(`./app/dist/assets/img/favicon/${c.name}`, c.contents);
    });
  };

favicons(faviconSrc, faviconCfg, faviconCb);
