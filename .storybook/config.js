import {configure} from '@kadira/storybook';

import 'style!raw!stylus!../app/styles/main.styl';

const loadStories = () => {
  require('../app/dist/js/stories/input.story.js');
};

configure(loadStories, module);
