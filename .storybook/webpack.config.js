const path = require('path');

module.exports = {
  module: {
    loaders: [{
      test: /\.styl$/,
      exclude: `node_modules/**`,
      loaders: ['style-loader!css-loader!stylus-loader', `raw`],
      include: path.resolve(`${__dirname}/app/styles`)
    }]
  }
};
