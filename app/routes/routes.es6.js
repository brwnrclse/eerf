import ip from 'ip';

const
  api = (req, res) => {
    res.sendFile(`${__dirname}/app/dist/doc/index.html`);
  },
  dev = (req, res) => {
    res.render(`dev`);
  },
  error = (err, req, res, next) => {
    res.status(500);
    res.render(`500`, {error: err, title: `500: Internal Server Error`});
  },
  fountain = (req, res) => {
    res.render(`fountain`);
  },
  index = (req, res) => {
    (process.env.NODE_ENV === `production`) ?
        res.render(`index`,
            {prod: true}) : res.render(`index`,
            {prod: false, addr: ip.address()});
  },
  notFound = (req, res) => {
    res.status(404);
    res.render(`404`, {title: `404: Resource not found`});
  };

export default {
  api,
  dev,
  error,
  fountain,
  index,
  notFound
};
