import {withPackageName} from 'react-pacomo';

const {transformer: pacomoTransformer} = withPackageName('eerf');

export default pacomoTransformer;
