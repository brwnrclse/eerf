import React from 'react';
import {storiesOf} from '@kadira/storybook';

import Input from '../components/input.js';

storiesOf(`Input`, module)
  .add(`with place holder`, () => {
    const
      dummy = () => {
        console.log(`change is a happenin`);
      },
      dummyVal = `story starts now...`;

    return (
      <Input
          handleChange={dummy}
          placeHolderValue={dummyVal}
      />
    );
  })
  .add(`with no place holder`, () => {
    const
      dummy = () => {
        console.log(`change is a happenin, place holder or not`);
      };

    return (
      <Input handleChange={dummy} />
    );
  });
