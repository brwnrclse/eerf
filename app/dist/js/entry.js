import React from 'react';
import {render} from 'react-dom';

import eerf from './layouts/app.js';
import eerfStore from './stores/eerf-store.js';

render(React.createElement(eerf, {store: eerfStore}),
    document.querySelector(`#eerf-root`));
