import React, {PropTypes} from 'react';
import pacomoTransformer from '../util/pacomo.js';

import Input from '../components/input.js';
import Output from '../components/output.js';

/**
 * Content layout component
 *
 * @param      {Object} props Object containing state
 * @param      {Object} props.store our state accessor / modifiers
 * @return     {React.Component} vdom representation of our component
 */
const Content = ({store}) => {
  return (
    <main>
      <Input
          handleChange={store.updateStoreData}
          placeHolderValue={store.getPlaceHolder()}
      />
      <Output value={store.getOutput()}/>
    </main>
  );
};

Content.displayName = `Content`;
Content.propTypes = {
  store: PropTypes.objectOf(
    PropTypes.func,
    PropTypes.func,
    PropTypes.func
  )
};

export default pacomoTransformer(Content);
