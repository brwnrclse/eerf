import React, {PropTypes} from 'react';

import pacomoTransformer from '../util/pacomo.js';
import Navbar from '../components/navbar/navbar.js';

/**
 * Header element, containig options and menu
 *
 * @param      {Object}   props          Object containing state
 * @param      {Object[]} props.navItems List of items for the nav menu
 * @return     {React.Component} a vdom representation of our component
 */
const Header = ({navItems}) => {
  return (
    <header>
      <span className={`appTitle`}>
        {`Eerf`}
      </span>
      <Navbar items={navItems}/>
    </header>
  );
};

Header.displayName = `Header`;
Header.propTypes = {
  navItems: PropTypes.arrayOf(PropTypes.Object).isRequired
};

export default pacomoTransformer(Header);
