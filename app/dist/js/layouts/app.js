import React, {PropTypes} from 'react';
import pacomoTransformer from '../util/pacomo.js';

import Content from './content.js';
import Header from './header.js';

/**
 * Simple container for our app layout.
 *
 * @param      {Object}      props                    Object containing state
 * @param      {Object}      props.store              Mobx store
 * @param      {function}    props.store.getNavItems  Retrieve navbar list items
 * @return     {React.Component}  A vdom representation of our app
 */
const App = ({store}) => {
  return (
    <div>
      <Header navItems={store.getNavItems()} />
      <Content store={store} />
    </div>
  );
};

App.displayName = `App`;
App.propTypes = {
  store: PropTypes.objectOf(
    PropTypes.func.isRequired
  )
};

export default pacomoTransformer(App);
