import {autorunAsync, observable} from 'mobx';
import fountainJS from 'fountain';
import navData from './navbar-data.js';

/**
 * Our Store factory for controlling app state
 *
 * @param      {Object}  fountain    Our fountain parser
 * @param      {Object}  eerfNavData Navbar action items w/ links
 * @return     {Object}              Our functions for mutating/accessing state
 */
const eerfStore = (fountain, eerfNavData) => {
  const
    eerfStoreData = observable({
      eerffountain: ``,
      eerfoutput: ``
    }),
    saveInterval = 1500,

    autosave = autorunAsync(() => {
      if (typeof(Storage) !== `undefined`) {
        localStorage.eerfstoreLS = JSON.stringify(eerfStoreData);
      }
    }, saveInterval),

    getPlaceHolder = () => {
      return `Type Something Here`;
    },

    getFountain = () => {
      return eerfStoreData.eerffountain;
    },

    getOutput = () => {
      return eerfStoreData.eerfoutput;
    },

    getNavItems = () => {
      return eerfNavData;
    },

    updateStoreData = (synEvent) => {
      const
        eerffountain = synEvent.target.value,
        eerfoutput = fountain.parse(eerffountain) || eerfStoreData.eerfoutput;

      eerfStoreData.eerffountain = eerffountain;
      eerfStoreData.eerfoutput =
          `${eerfoutput.title_page_html}${eerfoutput.script_html}`;
    };

  return {
    getPlaceHolder,
    getFountain,
    getOutput,
    getNavItems,
    updateStoreData
  };
};

export default eerfStore(fountainJS, navData);
