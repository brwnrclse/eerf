// data for the navbar
// Convert to flux architecture with mobx @1.0.0
const data = [
  {
    'text': `save`,
    'url': `#`
  },
  {
    'text': `upload`,
    'url': `#`
  }
];

export default data;
