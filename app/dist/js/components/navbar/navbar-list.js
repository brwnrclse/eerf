import React, {PropTypes} from 'react';
import pacomoTransformer from '../../util/pacomo.js';

import NavbarListItem from './navbar-item.js';

/**
 * Navbar list component
 *
 * @class      NavbarList
 * @param      {Object} props         Object containing state
 * @param      {Object[]} props.items List items to display
 * @return     {React.Component} vdom representation of our component
 */
const NavbarList = ({items}) => {
  return (
    <ul>
      {items.map(({text, url}) => {
        return (
          <NavbarListItem
              key={Math.random()}
              text={text}
              url={url}
          />
        );
      })}
    </ul>
  );
};

NavbarList.displayName = `Navbar-List`;
NavbarList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.Object)
};

export default pacomoTransformer(NavbarList);
