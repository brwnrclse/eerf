import React, {PropTypes} from 'react';

import pacomoTransformer from '../../util/pacomo.js';

/**
 * Styled item for Navbar Component
 *
 * @param      {Object}  props       Object containing containing state
 * @param      {string}  props.text  Display text for link
 * @param      {string}  props.url   Destination for link
 * @return     {Rect.Component}  A vdom representation of our app
 */
const NavbarItem = ({text, url}) => {
  return (
    <li>
      <a href={url}>
        {text}
      </a>
    </li>
    );
};

NavbarItem.displayName = `Navbar-ListItem`;
NavbarItem.propTypes = {
  text: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired
};

export default pacomoTransformer(NavbarItem);
