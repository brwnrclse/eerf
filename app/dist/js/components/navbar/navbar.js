import React, {PropTypes} from 'react';

import pacomoTransformer from '../../util/pacomo.js';
import NavbarList from './navbar-list.js';

/**
 * Navbar component for displaying app main menu and options
 *
 * @param      {Object} props         Object containing state
 * @param      {Object[]} props.items Navbar list entries
 * @return     {Rect.Component}  a vdom representation of our component
 */
const Navbar = ({items}) => {
  return (
    <nav>
      <NavbarList items={items} />
    </nav>
  );
};

Navbar.displayName = `Navbar`;
Navbar.propTypes = {
  items: PropTypes.arrayOf(PropTypes.Object).isRequired
};

export default pacomoTransformer(Navbar);
