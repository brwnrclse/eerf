import React, {PropTypes} from 'react';
import {observer} from 'mobx-react';
import pacomoTransformer from '../util/pacomo.js';

/**
 * Fountain input text area
 *
 * @param      {Object}    props                  Component props
 * @param      {function}  props.handleChange     Input handler
 * @param      {string}    props.placeHolderVlaue base value for input
 * @return     {React.Component}  A vdom representation of our app
 */
const Input = observer(({handleChange, placeHolderValue}) => {
  return (
    <div>
      <textarea
          className={`content`}
          onChange={handleChange}
          placeholder={placeHolderValue}
      />
    </div>
  );
});

Input.displayName = `Input`;
Input.propTypes = {
  handleChange: PropTypes.func.isRequired,
  placeHolderValue: PropTypes.string.isRequired
};

export default pacomoTransformer(Input);
