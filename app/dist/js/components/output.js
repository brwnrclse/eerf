import React, {PropTypes} from 'react';
import {observer} from 'mobx-react';
import pacomoTransformer from '../util/pacomo.js';

/**
 * Script view elememt
 *
 * @param      {Object}  props  Our elements props
 * @param      {string}  props.value  The html formatted script
 * @return     {React.Component}  A vdom representation of our element
 */
const Output = observer(({value}) => {
  return (
    <div dangerouslySetInnerHTML={{__html: value}}/>
  );
});

Output.displayName = `Output`;
Output.propTypes = {
  value: PropTypes.string.isRequired
};

export default pacomoTransformer(Output);
