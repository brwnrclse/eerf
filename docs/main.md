# eerf Docs

### Getting Started
Pull down the repo and go ahead and run npm install

``` bash

git clone https://bitbucket.org/brwnrclse/eerf

cd eerf && npm install
```

### Dev Scripts
After a successful install, you're faced with a few options:

``` javascript

build // Builds production ready app with favicons (build:server build:app build:favicon)

build:server // Builds the node server needed

build:favicon // Builds favicons from ico

clean // Cleans any auto-generated files

doc // Build documentation for modules using esdoc

jspm:install // Scoped usage of jspm, installs package with jspm

jspm:update // Scoped usage of jspm, updates packages 

start // Runs the app normally, ie node index.js

start:dev // Runs the app normally with hot-reloading enabled

start:storybook // Runs our client-side isolated dev env (react-storybook)

start:watch // Runs the app with hot-reloading and nodemon 

test // Runs test

```

### Working
#### Backend workflow
Most of the app is client-side but the backend does need tweaks from time to time.

Start by build the server, then watching it:

``` bash

npm run build:server

npm run jspm:install

npm run watch

```

Now you're good to go, any changes to routes, servers, or anything in our src/ will cause the app to auto reload!


#### Client-side workflow
Client-side is quite similar to backend.

Simply build the server and watch, this time with only hot-reloading:

``` bash
npm run build:server

npm run jspm:install

npm run start:dev

```

You should see a message in your terminal saying the app has started wih hot-reloading enabled.

#### Storybook workflow
Sometimes you just want to do alittle dev'n on a single component and dont want to render the entire app: enter [React Storybook](https://github.com/kadirahq/react-storybook)

Again build the server and run the storybook

``` bash
npm run build:server

npm run jspm:install

npm run start:storybook

```

Storybooks are located in `app/dist/js/stories`
The storybook config is located in `.storybook/`

#### Styling
CSS in JS is a great concept, but some of the most powerful battle-tested tools for crafting styles exist all ready, a la LESS, SCSS, STYLUS, etc...

With JSPM most css preprocessors are already supported for loading, except stylus, but there a simple workaround: *all preprocessors compile down to css*. So no matter how deeply nested you already were in preprocessor land you don't have to give that up because importing css into your react component is always gonna work:

```
// main.styl
$main-color = blue;

.App
 color $main-color

 &-component
    background-color red
    text-decoration italics
```

Turns into this

```
// main.css
.App: {
  color: blue;
}
.App-component: {
  color: blue;
  background-color: red;
}

```

Which can be imported simply as:

``` javascript
import styles from '../css/component.css!';

const Component = () => {
    return (
        <div style={styles.App}>
            <h3 style={styles[`App-component`]}>{`Hello World`}</h3>
        </div>
    );
}; 

export default Component;

```
